# AWS Deploy Lambda

AWS Deploy Lambda will deploy a serverless function to AWS Lambda.

## Usage

```yml
include:
  - component: gitlab.com/tickett/components/aws-deploy-lambda/aws-deploy-lambda@main
    inputs:
      build_job_name: build-lambda-function
      function_name: SuperAwesomeLambdaFunction
      zip_filename: release.zip

build-lambda-function:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  script:
    - dotnet publish -c Release --self-contained false -r linux-arm64 -o publish
    - cd publish
    - zip -r ../release.zip *
  artifacts:
    paths:
      - release.zip
```

## Inputs

### `build_job_name`

The name of the job to collect the zip file artifact from.

### `force_deploy`

* Leave blank to trigger a deployment when creating a tag.
* Set to `true` to force a deployment when not creating a tag.

### `function_name`

The function name is used to know where to deploy in AWS.
It is also used as a suffix on the job name to allow this CI component to be
imported in the same `.gitlab-ci.yml` multiple times (for different environments
for example).

### `use_oidc`

The identity provider:

* Leave blank to use AWS access key / secreate.
* Set to `true` to use GitLab IdP for AWS.

### `zip_filename`

The filename for the archive containing the code to deploy to AWS Lambda.

## Variables

The AWS CLI needs to authenticate with AWS.
Configure these as project CI/CD variables and ensure they are masked/protected.

* `AWS_DEFAULT_REGION`.

### When Using OIDC

* `ROLE_ARN`:
  The AWS ARN for the role to assume.
* `AWS_PROFILE`: `oidc`.
* `AWS_CONFIG_FILE`:

  ```
  [profile oidc]
  role_arn=${ROLE_ARN}
  web_identity_token_file=${OIDC_TOKEN}
  ```

The role trust relationship should be configured similar to:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::<ACCOUNT_ID>:oidc-provider/gitlab.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "gitlab.com:aud": "https://gitlab.com"
                },
                "StringLike": {
                    "gitlab.com:sub": "project_path:group-path/project-path:*"
                }
            }
        }
    ]
}
```

### When not Using OIDC

* `AWS_ACCESS_KEY_ID`.
* `AWS_SECRET_ACCESS_KEY`.

Use an IAM user with a deciated policy to execute a single action: `lambda:UpdateFunctionCode`
on a single resource (the AWS Lambda function):

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "DeployLambda",
            "Effect": "Allow",
            "Action": "lambda:UpdateFunctionCode",
            "Resource": "arn:aws:lambda:<REGION>:<ACCOUNT_ID>:function:<FUNCTION_NAME>"
        }
    ]
}
```
